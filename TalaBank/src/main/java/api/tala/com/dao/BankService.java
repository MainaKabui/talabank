package api.tala.com.dao;

import api.tala.com.model.DepositRequest;
import api.tala.com.model.TransactionResponse;
import api.tala.com.model.WithDrawalRequest;

public interface BankService {

	public TransactionResponse getBalance(String accountNumber);

	public TransactionResponse createDeposit(DepositRequest request);

	public TransactionResponse createWithdrawal(WithDrawalRequest request);

}