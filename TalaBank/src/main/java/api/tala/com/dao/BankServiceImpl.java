package api.tala.com.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.tala.com.model.Account;
import api.tala.com.model.DepositRequest;
import api.tala.com.model.Transaction;
import api.tala.com.model.TransactionResponse;
import api.tala.com.model.WithDrawalRequest;
import api.tala.com.util.FactoryUtil;
import api.tala.com.util.Props;

/**
 * <h1>Header</h1>
 * 
 * <p>
 * This is the implemenation class of the BankService interface that handles the
 * processing of the client requests which are passed via the BankController
 * class
 * </p>
 * 
 * 
 * @author {@link <a href="mickabui@gmail.com">Maina Kabui</a>}
 * @version 1.0.0.0
 * @since 2016-09-11
 */
@Service
public class BankServiceImpl implements BankService {

	@Autowired
	private Props props;

	private static final Logger logger = Logger.getLogger(BankServiceImpl.class
			.getName());

	/**
	 * This method processes a balance request via processing a
	 * {@link DepositRequest} object to return a {@link TransactionResponse}
	 * object that contains the transaction status
	 * 
	 * @param request
	 *            the {@link DepositRequest} object
	 * @return response a {@link TransactionResponse} object
	 */
	public TransactionResponse getBalance(String accountNumber) {
		TransactionResponse response = new TransactionResponse();

		EntityManager em = null;
		EntityTransaction transaction = null;
		Query query = null;

		Boolean isSuccessful = false;
		StringBuilder sb = new StringBuilder();

		try {
			logger.debug(String.format("Processing a new Request %s",
					accountNumber));

			if (accountNumber == null || accountNumber.equals("")) {
				sb.append("Invalid Account");

				throw new Exception(sb.toString());
			}

			em = FactoryUtil.factory().createEntityManager();
			transaction = em.getTransaction();
			transaction.begin();

			query = em.createNativeQuery(props.getAccountsSQL(), Account.class);
			query.setParameter(1, accountNumber);
			@SuppressWarnings("unchecked")
			List<Account> accountObject = query.getResultList();

			em.flush();

			if (accountObject != null && accountObject.size() > 0) {
				// The account details have been retrieved successfully
				logger.debug("The customer's account has been found");

				Account account = (Account) accountObject.get(0);
				sb.append(account.getCurrentBal());
			} else {
				// The account was not found
				logger.debug(String.format("Account not found", accountNumber));

				sb.append("Account not found");
			}

			transaction.commit();
		} catch (Exception ex) {
			logger.error(ex);

			if (sb.toString().equals(""))
				sb.append(String.format("System error %s", ex.getMessage()));
		} finally {
			if (em != null) {
				try {
					em.close();
				} catch (Exception ex) {
					logger.error(ex);
				}
			}
		}

		if (isSuccessful) {
			response.setStatusCode(props.getSuccessCode());
		} else {
			response.setStatusCode(props.getFailedCode());
		}

		response.setStatusMessage(sb.toString());

		logger.info(String.format("Processed the request. %s",
				response.toString()));

		return response;
	}

	/**
	 * This method processes a deposit request via processing a
	 * {@link DepositRequest} object to return a {@link TransactionResponse}
	 * object that contains the transaction status
	 * 
	 * @param request
	 *            the {@link DepositRequest} object
	 * @return response a {@link TransactionResponse} object
	 */
	public TransactionResponse createDeposit(DepositRequest request) {
		TransactionResponse response = new TransactionResponse();

		String accountNumber = request.getAccountNumber();
		Double newRequest = request.getAmount();
		String narrative = request.getNarrative();

		EntityManager em = null;
		EntityTransaction transaction = null;
		Query query = null;

		Boolean isSuccessful = false;
		String transactionType = "DEPOSIT";
		StringBuilder sb = new StringBuilder();

		try {
			logger.debug(String.format("Processing a new Request %s",
					request.toString()));

			// Validate the new request values
			if (accountNumber == null || accountNumber.equals("")
					|| newRequest == null || newRequest <= 0) {
				if (accountNumber == null || accountNumber.equals(""))
					sb.append("Invalid Account").append(", ");

				if (newRequest == null || newRequest <= 0)
					sb.append("Invalid Amount").append(", ");

				throw new Exception(sb.toString());
			} else if (request.getAmount() > props.getDailyDeposits()) {
				sb.append("Exceeded Maximum Deposit Per Transaction");

				throw new Exception(sb.toString());
			}

			// Initialize the entity manager object
			em = FactoryUtil.factory().createEntityManager();
			transaction = em.getTransaction();
			transaction.begin();

			query = em.createNativeQuery(props.getAccountsSQL(), Account.class);
			query.setParameter(1, accountNumber);
			@SuppressWarnings("unchecked")
			List<Account> accountObject = query.getResultList();

			em.flush();

			if (accountObject != null && accountObject.size() > 0) {
				// The account details have been retrieved successfully
				logger.debug("The customer's account has been found");

				Account account = (Account) accountObject.get(0);

				int currentTally = 1;
				Double currentTotal = newRequest;

				query = em.createNativeQuery(props.getTransactionsSQL());
				query.setParameter(1, account.getAccountNumber());
				query.setParameter(2, transactionType);
				query.setParameter(3, new java.sql.Date(new Date().getTime()));

				@SuppressWarnings("unchecked")
				List<Object[]> transactionObject = query.getResultList();

				em.flush();

				if (transactionObject != null && transactionObject.size() > 0) {
					// The customer has performed at least one transaction today
					logger.debug("The customer has performed at least one transaction today");

					Object obj = null;

					obj = transactionObject.get(0)[0];

					if (obj != null)
						currentTally += (Integer) obj;

					obj = transactionObject.get(0)[1];

					if (obj != null)
						currentTotal += (Double) obj;
				}

				if (currentTally <= props.getTotalNoDeposits()
						&& currentTotal <= props.getTotalDeposits()) {
					// The customer has not yet reached the day's ceiling
					logger.debug("The customer has not yet reached the day's ceiling");

					Transaction bankTransaction = new Transaction();

					bankTransaction.setAccount(account);
					bankTransaction.setAmount(newRequest);
					bankTransaction.setCurrencyCode(account.getCurrencyCode());
					bankTransaction.setNarrative(narrative);
					bankTransaction.setTransactionType(transactionType);
					bankTransaction.setDateCreated(new java.sql.Date(new Date()
							.getTime()));

					// Commit the new transaction
					em.persist(bankTransaction);
					em.flush();

					logger.debug(String
							.format("The debit transaction has been completed successfully. Reference No %s",
									bankTransaction.getAccount()));

					// Commit changes to the customer's account current balance
					Double currentBal = account.getCurrentBal();
					currentBal += newRequest;

					account.setCurrentBal(currentBal);
					em.persist(account);
					em.flush();

					logger.debug(String.format(
							"Account No: %s has been credited successfully.",
							account.getAccountNumber()));

					sb.append(String
							.format("The deposit has been completed successfully. Reference No %s",
									bankTransaction.getReferenceNo()));

					isSuccessful = true;
				} else {
					// The customer's request exceeds the day's ceiling
					logger.debug("The customer's request exceeds the day's ceiling");

					if (currentTally > props.getTotalNoDeposits())
						sb.append(
								"The maximum number of deposits per day has been exceed")
								.append(", ");

					if (currentTotal > props.getTotalDeposits())
						sb.append(
								"The daily maximum transaction amount has been exceed")
								.append(", ");
				}
			} else {
				// The account was not found
				logger.debug(String.format("Account not found",
						request.getAccountNumber()));

				sb.append("Account not found");
			}

			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				try {
					transaction.rollback();
				} catch (Exception e) {
					logger.error(e);
				}
			}

			logger.error(ex);

			if (sb.toString().equals(""))
				sb.append(String.format("System error %s", ex.getMessage()));
		} finally {
			if (em != null) {
				try {
					em.close();
				} catch (Exception ex) {
					logger.error(ex);
				}
			}
		}

		if (isSuccessful) {
			response.setStatusCode(props.getSuccessCode());
		} else {
			response.setStatusCode(props.getFailedCode());
		}

		response.setStatusMessage(sb.toString());

		logger.info(String.format("Processed the request. %s",
				response.toString()));

		return response;
	}

	/**
	 * This method processes a withdrawal request via processing a
	 * {@link WithDrawalRequest} object to return a {@link TransactionResponse}
	 * object that contains the transaction status
	 * 
	 * @param request
	 *            the {@link WithDrawalRequest} object
	 * @return response a {@link TransactionResponse} object
	 */
	public TransactionResponse createWithdrawal(WithDrawalRequest request) {
		TransactionResponse response = new TransactionResponse();

		String accountNumber = request.getAccountNumber();
		Double newRequest = request.getAmount();
		String narrative = request.getNarrative();

		EntityManager em = null;
		EntityTransaction transaction = null;
		Query query = null;

		Boolean isSuccessful = false;
		String transactionType = "WITHDRAWAL";
		StringBuilder sb = new StringBuilder();

		try {
			logger.debug(String.format("Processing a new Request %s",
					request.toString()));

			// Validate the new request values
			if (accountNumber == null || accountNumber.equals("")
					|| newRequest == null || newRequest <= 0) {
				if (accountNumber == null || accountNumber.equals(""))
					sb.append("Invalid Account").append(", ");

				if (newRequest == null || newRequest <= 0)
					sb.append("Invalid Amount").append(", ");

				throw new Exception(sb.toString());
			} else if (request.getAmount() > props.getDailyWithdrawals()) {
				sb.append("Exceeded Maximum Withdrawal Per Transaction");

				throw new Exception(sb.toString());
			}

			// Initialize the entity manager object
			em = FactoryUtil.factory().createEntityManager();
			transaction = em.getTransaction();
			transaction.begin();

			query = em.createNativeQuery(props.getAccountsSQL(), Account.class);
			query.setParameter(1, request.getAccountNumber());
			@SuppressWarnings("unchecked")
			List<Account> accountObject = query.getResultList();

			em.flush();

			if (accountObject != null && accountObject.size() > 0) {
				// The account details have been retrieved successfully
				Account account = (Account) accountObject.get(0);

				int currentTally = 1;
				Double currentTotal = newRequest;

				query = em.createNativeQuery(props.getTransactionsSQL());
				query.setParameter(1, accountNumber);
				query.setParameter(2, transactionType);
				query.setParameter(3, new java.sql.Date(new Date().getTime()));

				@SuppressWarnings("unchecked")
				List<Object[]> transactionObject = query.getResultList();

				em.flush();

				if (transactionObject != null && transactionObject.size() > 0) {
					// The customer has performed at least one transaction today
					logger.debug("The customer has performed at least one transaction today");

					Object obj = null;

					obj = transactionObject.get(0)[0];

					if (obj != null)
						currentTally += (Integer) obj;

					obj = transactionObject.get(0)[1];

					if (obj != null)
						currentTotal += (Double) obj;
				}

				if (currentTally <= props.getTotalNoWithdrawals()
						&& currentTotal <= props.getTotalWithdrawals()) {
					// The customer has not yet reached the day's ceiling
					logger.debug("The customer has not yet reached the day's ceiling");

					Transaction bankTransaction = new Transaction();

					bankTransaction.setAccount(account);
					bankTransaction.setAmount(newRequest);
					bankTransaction.setNarrative(narrative);
					bankTransaction.setCurrencyCode(account.getCurrencyCode());
					bankTransaction.setTransactionType(transactionType);
					bankTransaction.setDateCreated(new java.sql.Date(new Date()
							.getTime()));

					// Commit the new transaction
					em.persist(bankTransaction);
					em.flush();

					logger.debug(String
							.format("The withdrawal has been completed successfully. Reference No %s",
									bankTransaction.getAccount()));

					// Commit changes to the customer's account current balance
					Double currentBal = account.getCurrentBal();
					currentBal -= newRequest;

					account.setCurrentBal(currentBal);
					em.persist(account);
					em.flush();

					logger.debug(String.format(
							"Account No: %s has been debited successfully.",
							account.getAccountNumber()));

					sb.append(String
							.format("The debit transaction has been completed successfully. Reference No %s",
									bankTransaction.getReferenceNo()));

					isSuccessful = true;
				} else {
					// The customer's request exceeds the day's ceiling
					logger.debug("The customer's request exceeds the day's ceiling");

					if (currentTally > props.getTotalNoWithdrawals())
						sb.append(
								"The maximum number of withdrawals per day has been exceed")
								.append(", ");

					if (currentTotal > props.getTotalWithdrawals())
						sb.append(
								"The daily maximum transaction amount has been exceed")
								.append(", ");
				}
			} else {
				// The account was not found
				logger.debug(String.format("Account not found",
						request.getAccountNumber()));

				sb.append("Account not found");
			}

			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				try {
					transaction.rollback();
				} catch (Exception e) {
					logger.error(e);
				}
			}

			logger.error(ex);

			if (sb.toString().equals(""))
				sb.append(String.format("System error %s", ex.getMessage()));
		} finally {
			if (em != null) {
				try {
					em.close();
				} catch (Exception ex) {
					logger.error(ex);
				}
			}
		}

		if (isSuccessful) {
			response.setStatusCode(props.getSuccessCode());
		} else {
			response.setStatusCode(props.getFailedCode());
		}

		response.setStatusMessage(sb.toString());

		logger.info(String.format("Processed the request. %s",
				response.toString()));

		return response;

	}
}