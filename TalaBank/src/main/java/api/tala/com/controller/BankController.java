package api.tala.com.controller;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.WebAsyncTask;

import api.tala.com.dao.BankService;
import api.tala.com.model.DepositRequest;
import api.tala.com.model.TransactionResponse;
import api.tala.com.model.WithDrawalRequest;
import api.tala.com.util.Props;

/**
 * <h1>Header</h1>
 * 
 * <p>
 * This class contains handler methods which control the application's 3
 * endpoints i.e. the balance, deposit and withdrawal endpoints.
 * </p>
 * 
 * 
 * @author {@link <a href="mickabui@gmail.com">Maina Kabui</a>}
 * @version 1.0.0.0
 * @since 2016-09-11
 */
@RestController
public class BankController {

	@Autowired
	private BankService bankService;

	@Autowired
	Props props;

	private static final Logger logger = Logger.getLogger(BankController.class
			.getName());

	/**
	 * This is a non-blocking thread method that processes customer's balance
	 * requests.
	 * 
	 * @param accountNumber
	 *            the customer's account number
	 * 
	 * @return task i.e a {@link WebAsyncTask} object
	 */
	@RequestMapping(value = "api/v1/balance", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public WebAsyncTask<TransactionResponse> processBalanceRequest(
			@RequestParam(value = "accountNumber", defaultValue = "") final String accountNumber) {

		WebAsyncTask<TransactionResponse> task = null;

		task = new WebAsyncTask<TransactionResponse>(
				props.getTransactionTimeOut(),
				new Callable<TransactionResponse>() {
					@Override
					public TransactionResponse call() throws Exception {
						return bankService.getBalance(accountNumber);
					}

				});

		task.onTimeout(new Callable<TransactionResponse>() {

			@Override
			public TransactionResponse call() throws Exception {
				logger.warn("The request timed out");

				TransactionResponse response = new TransactionResponse();
				response.setStatusCode(props.getTimeoutCode());
				response.setStatusMessage(props.getEnquiryTimeoutMessage());

				return response;
			}

		});

		return task;
	}

	/**
	 * This is a non-blocking thread method that handles account deposits.
	 * 
	 * @param request
	 *            the {@link DepositRequest} object that contains the deposit
	 *            information
	 * 
	 * @return task i.e a WebAsync object
	 */
	@RequestMapping(value = "api/v1/deposit", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public WebAsyncTask<TransactionResponse> processDeposit(
			@RequestBody final DepositRequest request) {
		WebAsyncTask<TransactionResponse> task = null;

		task = new WebAsyncTask<TransactionResponse>(
				props.getTransactionTimeOut(),
				new Callable<TransactionResponse>() {

					@Override
					public TransactionResponse call() throws Exception {
						//
						return bankService.createDeposit(request);
					}

				});

		task.onTimeout(new Callable<TransactionResponse>() {

			@Override
			public TransactionResponse call() throws Exception {
				logger.warn("The request timed out.");

				TransactionResponse response = new TransactionResponse();
				response.setStatusCode(props.getTimeoutCode());
				response.setStatusMessage(props.getTransactionTimeoutMessage());

				return response;
			}
		});

		return task;
	}

	/**
	 * This processes customer's withdrawal requests asynchronously
	 * 
	 * @param request
	 *            the {@link WithDrawalRequest} object that contains the deposit
	 *            information
	 * 
	 * @return task i.e a WebAsync object
	 */
	@RequestMapping(value = "api/v1/withdrawal", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public WebAsyncTask<TransactionResponse> processWithdrawal(
			@RequestBody final WithDrawalRequest request) {

		WebAsyncTask<TransactionResponse> task = null;

		task = new WebAsyncTask<TransactionResponse>(
				props.getTransactionTimeOut(),
				new Callable<TransactionResponse>() {

					@Override
					public TransactionResponse call() throws Exception {
						return bankService.createWithdrawal(request);
					}

				});

		task.onTimeout(new Callable<TransactionResponse>() {

			@Override
			public TransactionResponse call() throws Exception {
				logger.warn("The request timed out");

				TransactionResponse response = new TransactionResponse();
				response.setStatusCode(props.getTimeoutCode());
				response.setStatusMessage(props.getTransactionTimeoutMessage());

				return response;
			}
		});

		return task;
	}
}
