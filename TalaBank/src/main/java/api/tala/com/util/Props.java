package api.tala.com.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:config.properties")
public class Props {

	@Value("${enquiry.timeout}")
	private Long enquiryTimeOut;

	@Value("${transaction.timeout}")
	private Long transactionTimeOut;

	@Value("${status.success}")
	private String successCode;

	@Value("${status.failed}")
	private String failedCode;

	@Value("${status.timeout}")
	private String timeoutCode;

	@Value("${status.bad.request}")
	private String badRequestCode;

	@Value("${enquiry.timeout.message}")
	private String enquiryTimeoutMessage;

	@Value("${transaction.timeout.message}")
	private String transactionTimeoutMessage;

	@Value("${query.accounts}")
	private String accountsSQL;

	@Value("${query.transactions}")
	private String transactionsSQL;

	@Value("${deposit.daily.tally}")
	private int totalNoDeposits;

	@Value("${deposit.daily.total}")
	private Double dailyDeposits;

	@Value("${deposit.total}")
	private Double totalDeposits;

	@Value("${withdrawal.daily.tally}")
	private int totalNoWithdrawals;

	@Value("${withdrawal.daily.total}")
	private int dailyWithdrawals;

	@Value("${withdrawal.total}")
	private Double totalWithdrawals;

	public Long getEnquiryTimeOut() {
		return enquiryTimeOut;
	}

	public void setEnquiryTimeOut(Long enquiryTimeOut) {
		this.enquiryTimeOut = enquiryTimeOut;
	}

	public Long getTransactionTimeOut() {
		return transactionTimeOut;
	}

	public void setTransactionTimeOut(Long transactionTimeOut) {
		this.transactionTimeOut = transactionTimeOut;
	}

	public String getSuccessCode() {
		return successCode;
	}

	public void setSuccessCode(String successCode) {
		this.successCode = successCode;
	}

	public String getFailedCode() {
		return failedCode;
	}

	public void setFailedCode(String failedCode) {
		this.failedCode = failedCode;
	}

	public String getTimeoutCode() {
		return timeoutCode;
	}

	public void setTimeoutCode(String timeoutCode) {
		this.timeoutCode = timeoutCode;
	}

	public String getBadRequestCode() {
		return badRequestCode;
	}

	public void setBadRequestCode(String badRequestCode) {
		this.badRequestCode = badRequestCode;
	}

	public String getEnquiryTimeoutMessage() {
		return enquiryTimeoutMessage;
	}

	public void setEnquiryTimeoutMessage(String enquiryTimeoutMessage) {
		this.enquiryTimeoutMessage = enquiryTimeoutMessage;
	}

	public String getTransactionTimeoutMessage() {
		return transactionTimeoutMessage;
	}

	public void setTransactionTimeoutMessage(String transactionTimeoutMessage) {
		this.transactionTimeoutMessage = transactionTimeoutMessage;
	}

	public String getAccountsSQL() {
		return accountsSQL;
	}

	public void setAccountsSQL(String accountsSQL) {
		this.accountsSQL = accountsSQL;
	}

	public String getTransactionsSQL() {
		return transactionsSQL;
	}

	public void setTransactionsSQL(String transactionsSQL) {
		this.transactionsSQL = transactionsSQL;
	}

	public int getTotalNoDeposits() {
		return totalNoDeposits;
	}

	public void setTotalNoDeposits(int totalNoDeposits) {
		this.totalNoDeposits = totalNoDeposits;
	}

	public Double getDailyDeposits() {
		return dailyDeposits;
	}

	public void setDailyDeposits(Double dailyDeposits) {
		this.dailyDeposits = dailyDeposits;
	}

	public Double getTotalDeposits() {
		return totalDeposits;
	}

	public void setTotalDeposits(Double totalDeposits) {
		this.totalDeposits = totalDeposits;
	}

	public int getTotalNoWithdrawals() {
		return totalNoWithdrawals;
	}

	public void setTotalNoWithdrawals(int totalNoWithdrawals) {
		this.totalNoWithdrawals = totalNoWithdrawals;
	}

	public int getDailyWithdrawals() {
		return dailyWithdrawals;
	}

	public void setDailyWithdrawals(int dailyWithdrawals) {
		this.dailyWithdrawals = dailyWithdrawals;
	}

	public Double getTotalWithdrawals() {
		return totalWithdrawals;
	}

	public void setTotalWithdrawals(Double totalWithdrawals) {
		this.totalWithdrawals = totalWithdrawals;
	}

}
