package api.tala.com.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

/**
 * <h1>Header</h1>
 * 
 * <p>
 * This class handles the the EntityManagerFactory factory settings via JPA
 * class
 * </p>
 * 
 * 
 * @author {@link <a href="mickabui@gmail.com">Maina Kabui</a>}
 * @version 1.0.0.0
 * @since 2016-09-11
 */
public class FactoryUtil {

	private static final EntityManagerFactory factory = EntityManagerFactory();

	private static final Logger logger = Logger
			.getLogger(EntityManagerFactory.class);

	private static EntityManagerFactory EntityManagerFactory() {
		try {
			return Persistence.createEntityManagerFactory("TalaBank");
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static EntityManagerFactory factory() {
		return factory;
	}

	public static void shutdown() {
		logger.info("Shuting down the connection factory");

		factory.close();

		logger.info("The connection factory has been shutdown");
	}
}