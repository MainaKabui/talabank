package api.tala.com.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the ACCOUNTS database table.
 * 
 */
@Entity
@Table(name = "ACCOUNTS")
@NamedQuery(name = "Account.findAll", query = "SELECT a FROM Account a")
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ACCOUNT_NUMBER", unique = true, nullable = false, length = 50)
	private String accountNumber;

	@Column(name = "ACCOUNT_NAME", length = 300)
	private String accountName;

	@Column(name = "ACCOUNT_STATUS")
	private int accountStatus;

	@Column(name = "CURRENCY_CODE", length = 3)
	private String currencyCode;

	@Column(name = "CURRENT_BAL")
	private double currentBal;

	@Column(name = "OPEN_BAL")
	private double openBal;

	// bi-directional many-to-one association to Transaction
	@OneToMany(mappedBy = "account")
	private List<Transaction> transactions;

	public Account() {
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getAccountStatus() {
		return this.accountStatus;
	}

	public void setAccountStatus(int accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public double getCurrentBal() {
		return this.currentBal;
	}

	public void setCurrentBal(double currentBal) {
		this.currentBal = currentBal;
	}

	public double getOpenBal() {
		return this.openBal;
	}

	public void setOpenBal(double openBal) {
		this.openBal = openBal;
	}

	public List<Transaction> getTransactions() {
		return this.transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Transaction addTransaction(Transaction transaction) {
		getTransactions().add(transaction);
		transaction.setAccount(this);

		return transaction;
	}

	public Transaction removeTransaction(Transaction transaction) {
		getTransactions().remove(transaction);
		transaction.setAccount(null);

		return transaction;
	}

}