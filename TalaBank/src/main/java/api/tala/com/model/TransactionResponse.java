package api.tala.com.model;

public class TransactionResponse {

	private String statusCode;
	private String statusMessage;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	@Override
	public String toString() {
		return "TransactionResponse [statusCode=" + statusCode
				+ ", statusMessage=" + statusMessage + "]";
	}

}
