package api.tala.com.model;

public class DepositRequest {

	private String accountNumber;
	private Double amount;
	private String narrative;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getNarrative() {
		return narrative;
	}

	public void setNarrative(String narrative) {
		this.narrative = narrative;
	}

	@Override
	public String toString() {
		return "DepositRequest [accountNumber=" + accountNumber + ", amount="
				+ amount + ", narrative=" + narrative + "]";
	}

}
