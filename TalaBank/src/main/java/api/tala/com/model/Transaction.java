package api.tala.com.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "TRANSACTIONS")
@NamedQuery(name = "Transaction.findAll", query = "SELECT t FROM Transaction t")
public class Transaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REFERENCE_NO", unique = true, nullable = false)
    private long referenceNo;

    @Column(name = "AMOUNT")
    private double amount;

    @Column(name = "CURRENCY_CODE", length = 3)
    private String currencyCode;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_CREATED")
    private Date dateCreated;

    @Column(name = "NARRATIVE", length = 1000)
    private String narrative;

    @Column(name = "TRANSACTION_TYPE", length = 50)
    private String transactionType;

    // bi-directional many-to-one association to Account
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_NUMBER")
    private Account account;

    public Transaction() {
    }

    public long getReferenceNo() {
	return this.referenceNo;
    }

    public void setReferenceNo(long referenceNo) {
	this.referenceNo = referenceNo;
    }

    public double getAmount() {
	return this.amount;
    }

    public void setAmount(double amount) {
	this.amount = amount;
    }

    public String getCurrencyCode() {
	return this.currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
	this.currencyCode = currencyCode;
    }

    public Date getDateCreated() {
	return this.dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
	this.dateCreated = dateCreated;
    }

    public String getNarrative() {
	return this.narrative;
    }

    public void setNarrative(String narrative) {
	this.narrative = narrative;
    }

    public String getTransactionType() {
	return this.transactionType;
    }

    public void setTransactionType(String transactionType) {
	this.transactionType = transactionType;
    }

    public Account getAccount() {
	return this.account;
    }

    public void setAccount(Account account) {
	this.account = account;
    }

}